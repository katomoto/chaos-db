# Chaos-DB

Test the way an application really behaves in the face of a flakey database. Simulates missing rows, high latency connection times, concurrency violations, and more. Inspired by the Netflix Chaos Monkey tool from 2012. See the `doc` directory for details on use.
